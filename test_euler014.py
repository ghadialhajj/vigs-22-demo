import euler014 as E

def test_collatz_len_returns_tuple():
    assert len(E.collatz_len(1)) == 2


def test_collatz_step_odd():
    assert E.collatz_step(3) == 7


def test_collatz_step_even():
    assert E.collatz_step(10) == 5
